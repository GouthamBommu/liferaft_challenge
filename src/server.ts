import express, {Application} from 'express';
import cors from 'cors';

import {PersonRoutes} from './presentation_layer/routes/persons.route';
import { CommonRoutesConfig } from './shared/common.routes';
import swaggerUi from "swagger-ui-express";

import swaggerDocument from './swagger.json';

const PORT = process.env.PORT || 3500
const app: Application = express();

const routes: Array<CommonRoutesConfig> = [];

app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use(express.json());
app.use(cors());


routes.push(new PersonRoutes(app));

app.listen(PORT, () => {
    console.log(`code-test service running on port ${PORT}`);
    routes.forEach((route: CommonRoutesConfig) => {
        route.configureRoutes();
        console.log(`Routes configured for ${route.getName()}`);
    });
});

