import { createMapper } from '@automapper/core';
import { classes } from '@automapper/classes';
import { personProfile } from './person.profile';

export const mapper = createMapper({
  name: 'projectmapper',
  pluginInitializer: classes,
});

mapper.addProfile(personProfile);
