import { CamelCaseNamingConvention, mapFrom } from '@automapper/core';
import { MappingProfile } from '@automapper/types';
import Person from "../../dal/models/person";
import {CreatePersonRequest} from "../../presentation_layer/requests/create.person"
import { PersonBaseRequest } from '../../presentation_layer/requests/person.base';
import { UpdatePersonRequest } from '../../presentation_layer/requests/update.person';
import { PersonResponse } from '../../presentation_layer/responses/get.person';

export const personProfile : MappingProfile = (mapper) => {

    mapper.createMap(PersonBaseRequest, Person, {
        namingConventions: {
            source: new CamelCaseNamingConvention(),
            destination: new CamelCaseNamingConvention(),
        }
    }).forMember(
        dest => dest.name,
        mapFrom(src => src.name)
    ).forMember(
        dest => dest.phoneNumber,
        mapFrom(src => src.phoneNumber)
    ).forMember(
        dest => dest.email,
        mapFrom(src => src.email)
    ).forMember(
        dest => dest.streetName,
        mapFrom(src => src.address.streetName)
    ).forMember(
        dest => dest.houseNumber,
        mapFrom(src => src.address.houseNumber)
    ).forMember(
        dest => dest.city,
        mapFrom(src => src.address.city)
    ).forMember(
        dest => dest.state,
        mapFrom(src => src.address.state)
    );

    mapper.createMap(CreatePersonRequest, Person, {extends: [mapper.getMapping(PersonBaseRequest, Person)]});

    mapper.createMap(UpdatePersonRequest, Person, {extends: [mapper.getMapping(PersonBaseRequest, Person)]})
    .afterMap((src, dest) => dest.id = src.id);

    mapper.createMap(Person, PersonResponse, {
        namingConventions: {
            source: new CamelCaseNamingConvention(),
            destination: new CamelCaseNamingConvention(),
        }
    }).forMember(
        dest => dest.id,
        mapFrom(src => src.id)
    ).forMember(
        dest => dest.name,
        mapFrom(src => src.name)
    ).forMember(
        dest => dest.phoneNumber,
        mapFrom(src => src.phoneNumber)
    ).forMember(
        dest => dest.email,
        mapFrom(src => src.email)
    ).forMember(
        dest => dest.address.streetName,
        mapFrom(src => src.streetName)
    ).forMember(
        dest => dest.address.houseNumber,
        mapFrom(src => src.houseNumber)
    ).forMember(
        dest => dest.address.city,
        mapFrom(src => src.city)
    ).forMember(
        dest => dest.address.state,
        mapFrom(src => src.state)
    );
}