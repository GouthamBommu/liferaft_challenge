'use strict';
const { Model } = require('sequelize');

interface PersonAttributes {
  id: number;
  name: string;
  phoneNumber: string;
  email: string;
  streetName: string;
  houseNumber: number;
  city: string;
  state: string;
}

export default class Person extends Model<PersonAttributes> 
        implements PersonAttributes {
    
     id!: number;
     name!: string;
     phoneNumber!: string;
     email!: string;
     streetName!: string;
     houseNumber!: number;
     city!: string;
     state!: string;    

    static associate(models: any) {
      // define association here
    }

    static init(sequelize: any, DataTypes: any): Person
    {
      return super.init({
        id: {
          type: DataTypes.BIGINT(11),
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false
        },
        phoneNumber: {
          type: DataTypes.STRING,
          allowNull: false
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true
        },
        streetName: {
          type: DataTypes.STRING,
          allowNull: false
        },
        houseNumber: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        city: {
          type: DataTypes.STRING,
          allowNull: false
        },
        state: {
          type: DataTypes.STRING,
          allowNull: false
        }
      }, {
        sequelize,
        modelName: 'Person'
      })
    }
}