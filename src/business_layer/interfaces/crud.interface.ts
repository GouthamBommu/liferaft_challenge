export interface CRUD {
    getAll: () => Promise<any>;
    create: (resource: any) => Promise<any>;
    update: (resource: any) => Promise<boolean>;
    getById: (id: number) => Promise<any>;
    deleteById: (id: number) => Promise<boolean>;
}