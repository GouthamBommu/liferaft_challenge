import db from "../../dal/database";
import Person from "../../dal/models/person";
import { CreatePersonRequest } from "../../presentation_layer/requests/create.person";
import { UpdatePersonRequest } from "../../presentation_layer/requests/update.person";
import { PersonResponse } from "../../presentation_layer/responses/get.person";
import {mapper} from "../../shared/mappers/mapper"
import {retryAsync} from "ts-retry"; 
import {CRUD} from "../interfaces/crud.interface"

class PersonService implements CRUD{

    public async getAll(): Promise<PersonResponse[]>
    {
        try{
            return await retryAsync(async () => {
                    let persons = await db.person.findAll();
                    return mapper.mapArray(persons, PersonResponse, Person);
                }, { delay:100, maxTry:3}
            );
        }
        catch(e)
        {
            console.log(e);
            throw e;
        }
    }

    public async getByEmail(email: string): Promise<PersonResponse>
    {
        try{

            return await retryAsync(async () => {
                    let person = await db.person.findOne({
                        where: {
                            email: email
                        }
                    });
                    return  mapper.map(person, PersonResponse, Person);
                }, { delay:100, maxTry:3}
            );
            
        }
        catch(e)
        {
            throw e;
        }
    }


    public async getById(id: number): Promise<PersonResponse>
    {
        try{

            return await retryAsync(async () => {
                    let person = await db.person.findOne({
                        where: {
                            id: id
                        }
                    });
                    return  mapper.map(person, PersonResponse, Person);
                }, { delay:100, maxTry:3}
            );
            
        }
        catch(e)
        {
            throw e;
        }
    }

    public async create(request: CreatePersonRequest): Promise<PersonResponse>
    {
        try{

            return await retryAsync(async () => {
                    let person = mapper.map(request, Person, CreatePersonRequest);
                    let dbPerson = await db.person.create(person.dataValues);
                    return  mapper.map(dbPerson, PersonResponse, Person);
                }, { delay:100, maxTry:3}
            );
            
        }
        catch(e)
        {
            throw e;
        }
    }

    public async update(request: UpdatePersonRequest): Promise<boolean>
    {
        try{

            return await retryAsync(async () => {
                    let person = mapper.map(request, Person, UpdatePersonRequest);
                    let response = await db.person.update(person.dataValues, {
                        where :{
                            id: request.id
                        }
                    });
                    return Boolean(response[0]);
                }, { delay:100, maxTry:3}
            );
        }
        catch(e)
        {
            throw e;
        }
    }

    public async deleteById(id: number): Promise<boolean>
    {
        try{
            return await retryAsync(async () => {
                    var response = await db.person.destroy({
                        where:{
                            id: id
                        }
                    });
                    return Boolean(response);
                }, { delay:100, maxTry:3}
            );
        }
        catch(e)
        {
            throw e;
        }
    }
}

export default new PersonService();