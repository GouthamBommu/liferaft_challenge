import express from 'express';
import personService from "../../business_layer/services/person.service";

class PersonController {

    async getPersons(req: express.Request, res: express.Response) {
        try{
            var persons = await personService.getAll();
            res.status(200).send(persons);
        }
        catch(e)
        {
            res.status(500).send({errors: [e.message]});
        }
    }

    async getPerson(req: express.Request, res: express.Response) {
        try{
            let id = parseInt(req.params.id);
            var person = await personService.getById(id);
            if(person){
                res.status(200).send(person);
            }
            else{
                res.status(404).send(person);
            }
        }
        catch(e)
        {
            res.status(500).send({errors: [e.message]});
        }
    }

    async createPerson(req: express.Request, res: express.Response) {
        try{
            var body = req.body;
            var person = await personService.create(body);
            res.status(201).send(person);
        }
        catch(e)
        {
            res.status(400).send({errors: [e.message]});
        }
    }

    async updatePerson(req: express.Request, res: express.Response) {
        try{
            var body = req.body;
            var response = await personService.update(body);
            res.status(204).send(response);
        }
        catch(e)
        {
            res.status(400).send({errors: [e.message]});
        }
    }

    async deletePerson(req: express.Request, res: express.Response) {
        try{
            var personId = parseInt(req.params.id);
            var response = await personService.deleteById(personId);
            res.status(204).send(response);
        }
        catch(e)
        {
            res.status(400).send({errors: [e.message]});
        }
    }
}

export default new PersonController();
