import express from "express";
import Joi from "joi";
import personService from "../../business_layer/services/person.service";

class PersonsValidator{

    // private baseSchema: Joi.ObjectSchema
    // constructor()
    // {
    //     this.baseSchema = Joi.object({
    //         name: Joi.string().required().trim().max(15),
    //         phoneNumber: Joi.string().required().length(10),
    //         email: Joi.string().required().email(),
    //         address: Joi.object({
    //             streetName: Joi.string().required().trim().max(20),
    //             houseNumber: Joi.number().required(),
    //             city: Joi.string().required(),
    //             state: Joi.string().required()
    //         })
    //     });
    // }

    async validatePersonWithIdExists(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction)
    {
        let {id} = req.params;
        let person = await personService.getById(parseInt(id));
        if(!person){
            res.status(404).send({errors: [`Person with Id:${parseInt(id)} not exists`]});
        }
        else{
            next();
        }
    }

    async validatePersonWithEmailExists(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction)
    {
        let person = await personService.getByEmail(req.body.email);
        let exists = person && person.id !== req.body.id 
        if(exists){
            res.status(400).send({errors: [`Person with ${req.body.email} already exists`]});
        }
        else{
            next();
        }
    }

    async validateCreatePersonRequest(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        let baseSchema = Joi.object({
            name: Joi.string().required().trim().max(15),
            phoneNumber: Joi.string().required().length(10),
            email: Joi.string().required().email(),
            address: Joi.object({
                streetName: Joi.string().required().trim().max(20),
                houseNumber: Joi.number().required(),
                city: Joi.string().required(),
                state: Joi.string().required()
            })
        });
        let createSchema = baseSchema;
        let result = createSchema.validate(req.body);
        if (!result.error) {
            next();
        } else {
            res.status(400).send({
                errors: result.error.details.map(d => d.message),
            });
        }
    }

    async validateUpdatePersonRequest(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        let baseSchema = Joi.object({
            name: Joi.string().required().trim().max(15),
            phoneNumber: Joi.string().required().length(10),
            email: Joi.string().required().email(),
            address: Joi.object({
                streetName: Joi.string().required().trim().max(20),
                houseNumber: Joi.number().required(),
                city: Joi.string().required(),
                state: Joi.string().required()
            })
        });
        let updateSchema = baseSchema.append({id: Joi.number().required()});
        let result = updateSchema.validate(req.body);
        if (!result.error) {
            next();
        } else {
            res.status(400).send({
                errors: result.error.details.map(d => d.message),
            });
        }
    }
}

export default new PersonsValidator();
