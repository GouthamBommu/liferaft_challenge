import { Address } from "./person.address";

export class PersonBaseRequest {
    name!: string;
    phoneNumber!: string;
    email!: string;
    address!: Address;
}