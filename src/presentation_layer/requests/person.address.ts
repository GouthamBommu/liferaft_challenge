export class Address {
    streetName!: string;
    houseNumber!: number;
    city!: string;
    state!: string;
}