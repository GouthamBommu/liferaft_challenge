import { Address } from "./person.address";
import { PersonBaseRequest } from "./person.base";

export class UpdatePersonRequest extends PersonBaseRequest {
    id! : number;
}