import express from 'express';
import { CommonRoutesConfig } from "../../shared/common.routes";
import personsValidator from '../middleware/persons.validator';
import personsController from '../controllers/persons.controller';

export class PersonRoutes extends CommonRoutesConfig {

    constructor(app: express.Application) {
        super(app, 'PersonRoutes');
    }

    configureRoutes(): express.Application {

        this.app.route(`/persons`)
            .get(personsController.getPersons)
            .post(
                personsValidator.validatePersonWithEmailExists,
                personsValidator.validateCreatePersonRequest,
                personsController.createPerson
            )
            .put(
                personsValidator.validatePersonWithIdExists,
                personsValidator.validateUpdatePersonRequest,
                personsValidator.validatePersonWithEmailExists,
                personsController.updatePerson
            );

        this.app.route(`/persons/:id`)
            .get(personsController.getPerson)
            .delete(
                personsValidator.validatePersonWithIdExists,
                personsController.deletePerson
            );

        return this.app;
    }
}