import { Address } from "../requests/person.address";

export class PersonResponse {
    id!: number;
    name!: string;
    phoneNumber!: string;
    email!: string;
    address!: Address;
}