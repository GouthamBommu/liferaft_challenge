# LifeRaft_Challenge

### Documentation:

I have built this service on express js typescript and Sequelize ORM, which is being used to create and manage mysql database.

### Install dependencies
- Install node js and npm on your system.
- Install docker and docker-compose on your system.
- Install the sequelize client (ORM library) to create and manage database: **npm install --save-dev sequelize-cli**


### How to build/run application
- Clone the repository on your system and open the root folder.
- Launch database and service as docker containers by follwoing below instructions
    - run command: **docker-compose up -d**
    - To view database: **http://localhost:8080/**
        - username: root
        - password: 192837
- Run database migrations to create tables in a mysql docker container.
    - run command: **npx sequelize-cli db:migrate**
- Now the system is configured for testing
- To test the solution, open the swagger ui by following below instructions:
    - **http://localhost:3500/swagger**
    - execute api's
    

